# wallet-api-v1

## Requirements

 - [Leiningen](https://leiningen.org/)
 - Java 8
 - Postgres 10

## Building

 - `lein deps`
 - `lein uberjar`

## Running

`java -jar target/wallet-api-0.1.0-SNAPSHOT-standalone.jar 38ENmTr2AD1avJrmmi9iM7PfS6nZVmuMKf`

## License

Copyright © 2019 Sandae Macalalag

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
